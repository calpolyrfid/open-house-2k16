//import com.pi4j.io.gpio.GpioController;
//import com.pi4j.io.gpio.GpioFactory;
//import com.pi4j.io.gpio.GpioPinDigitalInput;
//import com.pi4j.io.gpio.GpioPinDigitalOutput;
//import com.pi4j.io.gpio.PinPullResistance;
//import com.pi4j.io.gpio.RaspiPin;
//import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
//import com.pi4j.io.gpio.event.GpioPinListenerDigital;


//import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.GpioPinDigitalOutput;

public class OpenHouseGpio {

	public static final int TagPinBlue = 0;
	public static final int TagPinRed = 1;
	public static final int TagPinGreen = 2;

	protected GpioPinDigitalOutput[] pins;
	private GpioController gpio;

	public OpenHouseGpio() {
	
		System.out.println("GPIO setup started\n");
	
		pins = new GpioPinDigitalOutput[3];
		this.gpio = GpioFactory.getInstance();

		pins[0] = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "LED1", PinState.LOW);
		pins[1] = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "LED2", PinState.LOW);
		pins[2] = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "LED3", PinState.LOW);

		pins[0].low();
		pins[1].low();
		pins[2].low();

		System.out.println("GPIO should be set");

	
	}

	public void GpioToggleHoldFor(int milli, int pinNum) throws InterruptedException {
		this.pins[pinNum].high();
		Thread.sleep(milli);
		this.pins[pinNum].low();
		Thread.sleep(milli);
	}

	public static void main(String[] args) throws InterruptedException {
		OpenHouseGpio test = new OpenHouseGpio();
		test.GpioToggleHoldFor(1000, TagPinRed);
		test.GpioToggleHoldFor(1000, TagPinBlue);
		test.GpioToggleHoldFor(1000, TagPinGreen);
	}
}
