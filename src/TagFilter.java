import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

public class TagFilter {
   public static final int TAG_UNKNOWN = -1;
   public static final int TAG_BLUE = 1;
   public static final int TAG_GREEN = 2;
   public static final int TAG_RED = 3;
   public static final int TAG_YELLOW = 4;


   // TagList Locations
   private URL url_blue      = getClass().getResource("lists/RFID_list_blue.txt");
   private URL url_green     = getClass().getResource("lists/RFID_list_green.txt");
   private URL url_red       = getClass().getResource("lists/RFID_list_red.txt");
   private URL url_yellow    = getClass().getResource("lists/RFID_list_yellow.txt");

   // Built Lists from TXT resources
   private ArrayList<String> tags_blue;
   private ArrayList<String> tags_green;
   private ArrayList<String> tags_red;
   private ArrayList<String> tags_yellow;


   // Constructor
   public TagFilter() {
      tags_blue   = buildList(url_blue);
      tags_green  = buildList(url_green);
      tags_red    = buildList(url_red);
      tags_yellow = buildList(url_yellow);
   }

   // Build the list of color-defined tags
   private ArrayList<String> buildList(URL list) {
      ArrayList<String> tags = new ArrayList<String>();

      try {
         System.out.println("Building list for " + list.toString());
         FileInputStream fis = new FileInputStream(new File(list.toURI()));
         BufferedReader br = new BufferedReader(new InputStreamReader(fis));

         String line = null;
         while ((line = br.readLine()) != null) {
            tags.add(line.trim());
         }

         br.close();
      } catch (FileNotFoundException e) {
         System.out.println(e.toString());
      } catch (URISyntaxException e) {
         System.out.println(e.toString());
      } catch (IOException e) {
         System.out.println(e.toString());
      }

      return tags;
   }

   public ArrayList<String> getBlueTagList() {
      return tags_blue;
   }

   public ArrayList<String> getGreenTagList() {
      return tags_green;
   }

   public ArrayList<String> getRedTagList() {
      return tags_red;
   }

   public ArrayList<String> getYellowTagList() {
      return tags_yellow;
   }

   public int determineTagColor(String tag) {
      if (tags_blue.contains(tag)) {
         return TAG_BLUE;
      }
      else if (tags_green.contains(tag)) {
         return TAG_GREEN;
      }
      else if (tags_red.contains(tag)) {
         return TAG_RED;
      }
      else if (tags_yellow.contains(tag)) {
         return TAG_YELLOW;
      }

      return TAG_UNKNOWN;
   }

}
