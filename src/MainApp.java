import com.alien.enterpriseRFID.notify.*;
import com.alien.enterpriseRFID.reader.AlienClass1Reader;
import com.alien.enterpriseRFID.reader.AlienReaderException;
import com.alien.enterpriseRFID.tags.Tag;

import java.net.UnknownHostException;

public class MainApp {

    /*
    private class RFIDMessageListener implements MessageListener {
        @Override
        public void messageReceived(Message message){
            System.out.println("\nMessage Received:");
            if (message.getTagCount() == 0) {
                System.out.println("(No Tags)");
            } else {
                for (int i = 0; i < message.getTagCount(); i++) {
                    Tag tag = message.getTag(i);
                    System.out.println(tag.getTagID());
                }
            }
        }

    }*/

    public static void main(String[] args) throws AlienReaderException,
            UnknownHostException, InterruptedException {

        TagReaderGUI rfidGUI = new TagReaderGUI();
        rfidGUI.displayMessage("Initializing reader...");

        try {
            AlienClass1Reader reader =
                    AlienController.initializeReader("192.168.1.113", 23,
                            "alien", "password");
            boolean stop = false;
            TagFilter tf = new TagFilter();

            int numTags = 0;
            Tag tag;
            Tag lastTag = null;
            int color;

            while (!stop || (numTags < 20)) {
                tag = reader.getTag();

                if ((tag == lastTag) && tag != null)
                    break;

                rfidGUI.displayMessage("Scanning for tags...");
                if (tag != null) {
                    rfidGUI.displayMessage("Tag found!");
                    color = tf.determineTagColor(tag.getTagID());
                    switch (color) {
                        case TagFilter.TAG_BLUE:
                            System.out.println("Blue Tag Found");
                            rfidGUI.playVideo(TagReaderGUI.blueVideo, 8000);
                            break;
                        case TagFilter.TAG_GREEN:
                            System.out.println("Green Tag Found");
                            rfidGUI.playVideo(TagReaderGUI.greenVideo, 8000);
                            break;
                        case TagFilter.TAG_RED:
                            System.out.println("Red Tag Found");
                            rfidGUI.playVideo(TagReaderGUI.redVideo, 8000);
                            break;
                        case TagFilter.TAG_YELLOW:
                            System.out.println("Yellow Tag Found");
                            rfidGUI.playVideo(TagReaderGUI.yellowVideo, 8000);
                            break;
                        default:
                            System.out.println("Unknown Tag Found");
                    }
                    numTags++;
                    reader.clearTagList();
                    lastTag = tag;
                    tag = null;
                } else {
                    System.out.println("No Tag Found this cycle");
                }
                Thread.sleep(2000);
            }
        } catch (AlienReaderException e) {
            rfidGUI.displayMessage("Reader not found!");
        }
    }
}
