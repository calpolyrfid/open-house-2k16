import java.net.UnknownHostException;

import com.alien.enterpriseRFID.reader.AlienClass1Reader;
import com.alien.enterpriseRFID.reader.AlienReaderException;

public class AlienController {

   private static final int RF_LEVEL = 167;
   private static final String tagMask = "E200 XXXX XXXX XXXX XXXX XXXX";

   /**
    * Initialize reader settings and verify reader connection, set
    * @throws AlienReaderException 
    * @throws UnknownHostException 
    */
   public static AlienClass1Reader initializeReader(String ipAddress, int portNumber, String username, String password) throws UnknownHostException, AlienReaderException {
      // Establish user parameters
      AlienClass1Reader reader = new AlienClass1Reader(ipAddress, portNumber);
      reader.setUsername(username);
      reader.setPassword(password);

      // Establish connection and clear any initial tags read
      reader.open();
      reader.clearTagList();


      reader.setRFLevel(RF_LEVEL);
      reader.setTagMask(tagMask);
      reader.setTagListFormat(AlienClass1Reader.TEXT_FORMAT);
      reader.setTagStreamFormat(AlienClass1Reader.TEXT_FORMAT);
      //reader.setTagListMillis(AlienClass1Reader.ON);
      return reader;
   }
}
