import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class TagReaderGUI {

    /*
     * Video Locations
     */
    public static final String blueVideo =
            Paths.get("src/animations/blue_RFID.mp4").toUri().toString();
    public static final String greenVideo =
            Paths.get("src/animations/green_RFID.mp4").toUri().toString();
    public static final String redVideo =
            Paths.get("src/animations/red_RFID.mp4").toUri().toString();
    public static final String yellowVideo =
            Paths.get("src/animations/yellow_RFID.mp4").toUri().toString();

    /*
     * Window Size Constants
     */
    private final int WIDTH = 800;
    private final int HEIGHT = 450;

    private JLabel label;
    private JFXPanel jfxPanel;
    private Group root;
    private Scene scene;

    public TagReaderGUI() {
        final int FONT_SIZE = 40;

        JFrame frame = new JFrame("Tag Reader Demo");
        label = new JLabel("", SwingConstants.CENTER);
        label.setBackground(Color.white);
        label.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        label.setFont(new Font("Font", Font.PLAIN, FONT_SIZE));
        jfxPanel = new JFXPanel();

        frame.setSize(WIDTH, HEIGHT);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().add(label, BorderLayout.CENTER);
        frame.pack();
        frame.add(jfxPanel);

        clearGUI();
    }

    public void displayMessage(String message) {
        label.setText(message);
    }

    /*
     * This method plays a video from the "animations" directory. There is a
     * one second delay before the video plays, and each video plays for the
     * specified time in milliseconds before the GUI is cleared.
     */
    public void playVideo(String option, int videoDuration) {
        int VIDEO_DELAY = 1000;

        Media video = new Media(option);
        MediaPlayer player = new MediaPlayer(video);
        MediaView view = new MediaView(player);
        view.setFitWidth(WIDTH);
        view.setFitHeight(HEIGHT);

        Timer timer = new Timer(VIDEO_DELAY, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        label.setText("");
                        root.getChildren().add(view);
                        jfxPanel.setScene(scene);
                        player.play();
                    }
                });
            }
        });
        timer.setRepeats(false);
        timer.start();

        try {
            TimeUnit.MILLISECONDS.sleep(videoDuration);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        clearGUI();
    }

    private void clearGUI() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                root = new Group();
                scene = new Scene(root, WIDTH, HEIGHT);
                jfxPanel.setScene(scene);
            }
        });
    }
}
